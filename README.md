# Evaluation project - Weather Application
This project was created by me in order to solve evaluation task for C# Developer position.

## Solution structure
#### Weather.Api
Backend web application written in C# with use of ASP.NET WebApi2 framework.
Main parts:

* `Controllers/WeatherController.cs` - WebApi controller which contains weather requests entrypoint. It uses `Weather.Services` to process requests and return current weather forecast.
* `ViewModels/` - contains class definitions for view models which are data transfer objects used by application.
* `Mappings/` - contains mapping classes used to map from domain objects to data transfer objects.

#### Weather.Services
Class library with set of service classes which perform all logical operations.
Main parts:

* `Weather/IWeatherService.cs`, `Weather/WeatherService.cs` - weather service interface and its implementation. This service is responsible for providing weather forecast for desired location passed as parameter. It is done in following steps:

1. Using `CacheWeatherProvider`, check if weather forecast for location was previously fetched from third-party provider and did not expire.
2. If was, return cached  forecast.
3. If was not, fetch weather forecast from third-party provider and cache it.

* `WeatherProviders/CacheWeatherProvider` - as above, this service is used to cache forecasts. This class is configured to be singleton. It uses in-memory storage by static class property, so data is persistent only in life-time of application.
* `WeatherProviders/OpenWeatherMap` - weather provider implementation which uses `OpenWeatherMap` free API. Requests to `OpenWeatherMap` are secured with API key which was generated for purposes of this application.

#### Weather.Web
Frontend application written in TypeScript with use of React and Redux frameworks and a few helper libraries.
Main parts:

* `store/` - Redux store configuration which defines the application root state.
* `services/loader` - services responsible for showing or hiding loader when AJAX requests are performmed.
* `services/weather` - this service performs API calls to `Weather.Api`.
It uses `typescript-fsa` and `typescript-fsa-reducers` to create Redux action creators for AJAX request `started`, `done` and `failed`.
* `components/Form` - React component which displays form and dispatches Redux actions to initiate API call.
* `components/WeatherPresenter` - React component which presents the received forecast on webpage.
* `App.tsx` - main React components which controls displaying form or weather presenter.

#### Weather.Services.Tests.Unit
Unit tests for weather services

#### Weather.Services.Tests.Integration
* `OpenWeatherMap` provider integration tests.
* `WeatherController` integration tests.

## Requirements to run
* Visual Studio 2017
* NodeJS v8.11 or newer

## How to run
1. Open `Weather.sln` solution file with Visual Studio 2017
2. Restore solution NuGet packages and build solution
3. Run `Weather.Api` project
4. Backend application should be available on address http://localhost:51000/api/weather/{country}/{city}
5. `cmd` into `Weather.Web` folder
6. Run `npm i -g serve` to install simple NodeJS HTTP server
7. Run `serve -s dist` to run application
8. Frontend application should be available on address http://localhost:5000/
