﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using System.Web.Http;
using Weather.Api.Mappings;
using Weather.Services.Exceptions;
using Weather.Services.Weather;
using Weather.Services.Weather.Model;

namespace Weather.Api.Controllers
{
    public class WeatherController : ApiController
    {
        private readonly IWeatherService _weatherService;

        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        [HttpGet]
        [Route("api/weather/{country}/{city}")]
        public async Task<IHttpActionResult> GetForecast(string country, string city)
        {
            try
            {
                var forecast = await _weatherService.GetForecastAsync(new Location
                {
                    Country = country,
                    City = city
                });

                return Ok(forecast.ToViewModel());
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch (InvalidCredentialException e)
            {
                return InternalServerError(e);
            }
            catch (LocationNotFoundException)
            {
                return NotFound();
            }
            catch (WeatherServiceException e)
            {
                return InternalServerError(e);
            }
        }
    }
}
