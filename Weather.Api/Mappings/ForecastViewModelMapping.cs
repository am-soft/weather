﻿using Weather.Api.ViewModels;
using Weather.Services.Weather.Model;

namespace Weather.Api.Mappings
{
    public static class ForecastViewModelMapping
    {
        public static ForecastViewModel ToViewModel(this Forecast forecast)
        {
            var viewModel = new ForecastViewModel
            {
                Location = new LocationViewModel
                {
                    City = forecast.Location.City,
                    Country = forecast.Location.Country
                },
                Temperature = forecast.Temperature,
                Humidity = forecast.Humidity
            };

            return viewModel;
        }
    }
}