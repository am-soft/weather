﻿namespace Weather.Api.ViewModels
{
    public class LocationViewModel
    {
        public string Country { get; set; }

        public string City { get; set; }
    }
}