﻿using Weather.Services.Weather.Model;

namespace Weather.Api.ViewModels
{
    public class ForecastViewModel
    {
        public LocationViewModel Location { get; set; }

        public Temperature Temperature { get; set; }

        public decimal Humidity { get; set; }
    }
}