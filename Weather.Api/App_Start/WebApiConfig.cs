﻿using Autofac;
using Autofac.Integration.WebApi;
using Newtonsoft.Json.Serialization;
using System.Configuration;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;
using Weather.Services.Weather;
using Weather.Services.WeatherProviders;
using Weather.Services.WeatherProviders.CacheWeatherProvider;
using Weather.Services.WeatherProviders.OpenWeatherMap;

namespace Weather.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            config.Formatters.JsonFormatter.SupportedMediaTypes
                .Add(new MediaTypeHeaderValue("text/html"));

            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(corsAttr);

            ConfigureDependencyInjection(config);

            // Web API routes
            config.MapHttpAttributeRoutes();
        }


        private static void ConfigureDependencyInjection(HttpConfiguration config)
        {
            var openWeatherMapApiKey = ConfigurationManager.AppSettings["openWeatherMapApiKey"];

            var builder = new ContainerBuilder();

            builder.RegisterType<OpenWeatherMapProvider>().As<IWeatherProvider>().WithParameter("apiKey", openWeatherMapApiKey);
            builder.RegisterType<CacheWeatherProvider>().As<ICacheWeatherProvider>().SingleInstance();
            builder.RegisterType<WeatherService>().As<IWeatherService>();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
