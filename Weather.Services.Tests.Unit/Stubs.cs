﻿using NSubstitute;
using Weather.Services.Weather.Model;
using Weather.Services.WeatherProviders;
using Weather.Services.WeatherProviders.CacheWeatherProvider;

namespace Weather.Services.Tests
{
    public class Stubs
    {
        public static ICacheWeatherProvider CacheWeatherProvider => GetCacheWeatherProvider();

        private static ICacheWeatherProvider GetCacheWeatherProvider()
        {
            var cacheProvdier = Substitute.For<ICacheWeatherProvider>();
            cacheProvdier.GetForecastAsync(Arg.Any<Location>()).Returns((Forecast)null);

            return cacheProvdier;
        }

        public static IWeatherProvider WeatherProvider => Substitute.For<IWeatherProvider>();
    }
}
