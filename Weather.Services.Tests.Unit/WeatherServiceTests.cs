﻿using FluentAssertions;
using NSubstitute;
using System;
using System.Threading.Tasks;
using Weather.Services.Weather;
using Weather.Services.Weather.Model;
using Weather.Services.WeatherProviders;
using Xunit;

namespace Weather.Services.Tests
{
    public class WeatherServiceTests
    {
        [Theory]
        [InlineData("Poland", "Warsaw", "Celsius", 20, 40)]
        [InlineData("Polska", "Warszawa", "Celsius", 20, 40)]
        [InlineData("Germany", "Berlin", "Celsius", 15, 30)]
        [InlineData("Deutschland", "Berlin", "Celsius", 15, 30)]
        [InlineData("Russia", "Moscow", "Celsius", -10, 5)]
        [InlineData("Россия", "Москва", "Celsius", -10, 5)]
        public async Task Given_valid_location_results_forecast(string country, string city, string format, decimal temperature, decimal humidity)
        {
            var location = new Location
            {
                Country = country,
                City = city
            };

            var stubWeatherProvider = Substitute.For<IWeatherProvider>();
            stubWeatherProvider.GetForecastAsync(location).Returns(new Forecast
            {
                Location = location,
                Temperature = new Temperature
                {
                    Format = format,
                    Value = temperature
                },
                Humidity = humidity
            });

            var service = new WeatherService(stubWeatherProvider, Stubs.CacheWeatherProvider);
            var result = await service.GetForecastAsync(location);

            await stubWeatherProvider.Received().GetForecastAsync(Arg.Is(location));

            result.Should().NotBeNull();

            result.Location.Should().NotBeNull();
            result.Location.City.Should().BeSameAs(location.City);
            result.Location.Country.Should().BeSameAs(location.Country);

            result.Temperature.Should().NotBeNull();
            result.Temperature.Format.Should().BeSameAs(format);
            result.Temperature.Value.Should().Be(temperature);

            result.Humidity.Should().Be(humidity);
        }

        [Fact]
        public async Task Given_null_location_throws_exception()
        {
            var service = new WeatherService(Substitute.For<IWeatherProvider>(), Stubs.CacheWeatherProvider);

            await Assert.ThrowsAsync<ArgumentException>(() => service.GetForecastAsync(null));
        }

        [Fact]
        public async Task Given_empty_city_throws_exception()
        {
            var location = new Location
            {
                City = ""
            };

            var service = new WeatherService(Stubs.WeatherProvider, Stubs.CacheWeatherProvider);

            await Assert.ThrowsAsync<ArgumentException>(() => service.GetForecastAsync(location));
        }

        [Fact]
        public async Task Given_empty_country_throws_exception()
        {
            var location = new Location
            {
                Country = ""
            };

            var service = new WeatherService(Stubs.WeatherProvider, Stubs.CacheWeatherProvider);

            await Assert.ThrowsAsync<ArgumentException>(() => service.GetForecastAsync(location));
        }
    }
}
