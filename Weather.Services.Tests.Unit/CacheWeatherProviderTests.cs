﻿using FluentAssertions;
using System;
using Weather.Services.Weather.Model;
using Weather.Services.WeatherProviders.CacheWeatherProvider;
using Xunit;

namespace Weather.Services.Tests
{
    public class CacheWeatherProviderTests
    {
        [Theory]
        [InlineData("Poland", "Warsaw", 23.5, "Celsius", 55.4)]
        [InlineData("Poland", "Gdańsk", 24.5, "Celsius", 55)]
        [InlineData("United States", "New York", -12, "Celsius", 99)]
        public async void Given_valid_forecast_bad_date_results_null(string country, string city, decimal temperature, string format, decimal humidity)
        {
            var random = new Random(999);
            var hoursToAdd = random.Next(CacheWeatherProvider.MaxAcceptedForecastDateDifference.Hours + 1, 100);

            var forecast = new Forecast
            {
                Location = new Location
                {
                    City = city,
                    Country = country,
                },
                Temperature = new Temperature
                {
                    Format = format,
                    Value = temperature
                },
                Humidity = humidity,
                Date = DateTime.UtcNow.Add(new TimeSpan(hoursToAdd, 0, 0))
            };

            var cacheProvider = new CacheWeatherProvider();

            cacheProvider.CacheForecast(forecast);

            var result = await cacheProvider.GetForecastAsync(forecast.Location);

            result.Should().BeNull();

            cacheProvider.Dispose();
        }

        [Theory]
        [InlineData("Poland", "Warsaw", 23.5, "Celsius", 55.4)]
        [InlineData("Poland", "Gdańsk", 24.5, "Celsius", 55)]
        [InlineData("United States", "New York", -12, "Celsius", 99)]
        public async void Given_valid_forecast_results_cached_forecast(string country, string city, decimal temperature, string format, decimal humidity)
        {
            var random = new Random();
            var hoursToAdd = random.Next(0, CacheWeatherProvider.MaxAcceptedForecastDateDifference.Hours);

            var forecast = new Forecast
            {
                Location = new Location
                {
                    City = city,
                    Country = country,
                },
                Temperature = new Temperature
                {
                    Format = format,
                    Value = temperature
                },
                Humidity = humidity,
                Date = DateTime.UtcNow.Add(new TimeSpan(hoursToAdd, 0, 0))
            };

            var cacheProvider = new CacheWeatherProvider();

            cacheProvider.CacheForecast(forecast);

            var result = await cacheProvider.GetForecastAsync(forecast.Location);

            result.Should().BeEquivalentTo(forecast);

            cacheProvider.Dispose();
        }

        [Fact]
        public void Given_empty_forecast_throws_exception()
        {
            var forecast = new Forecast();

            var cacheProvider = new CacheWeatherProvider();

            Assert.Throws<ArgumentException>(() => cacheProvider.CacheForecast(forecast));
        }

        [Fact]
        public void Given_null_forecast_throws_exception()
        {
            var cacheProvider = new CacheWeatherProvider();

            Assert.Throws<ArgumentException>(() => cacheProvider.CacheForecast(null));
        }
    }
}
