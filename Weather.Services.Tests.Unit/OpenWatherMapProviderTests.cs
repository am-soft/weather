﻿using FluentAssertions;
using System;
using System.Security.Authentication;
using Weather.Services.Exceptions;
using Weather.Services.Weather.Model;
using Weather.Services.WeatherProviders.OpenWeatherMap;
using Xunit;

namespace Weather.Services.Tests
{
    public class OpenWatherMapProviderTests
    {
        private Location RandomLocation()
        {
            return new Location
            {
                City = Guid.NewGuid().ToString(),
                Country = Guid.NewGuid().ToString()
            };
        }

        [Theory]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Warsaw", "Poland")]
        public async void Given_valid_apiKey_and_location_results_forecast(string key, string city, string country)
        {
            var location = new Location
            {
                City = city,
                Country = country
            };

            var owmProvider = new OpenWeatherMapProvider(key);

            var result = await owmProvider.GetForecastAsync(location);

            result.Should().NotBeNull();

            result.Temperature.Should().NotBeNull();
            result.Temperature.Format.Should().NotBeEmpty();
        }

        [Theory]
        [InlineData("8a8b73944d61acc697760ed667fa1e56")]
        public async void Given_valid_apiKey_randomLocation_results_forecastDownloadResult_locationNotFound(string key)
        {
            var location = RandomLocation();

            var owmProvider = new OpenWeatherMapProvider(key);

            await Assert.ThrowsAsync<LocationNotFoundException>(async () => await owmProvider.GetForecastAsync(location));
        }

        [Theory]
        [InlineData("key")]
        public async void Given_invalid_apiKey_throws_exception(string key)
        {
            var location = RandomLocation();

            var owmProvider = new OpenWeatherMapProvider(key);

            await Assert.ThrowsAsync<InvalidCredentialException>(async () => await owmProvider.GetForecastAsync(location));
        }

        [Fact]
        public void Given_empty_apiKey_throws_exception()
        {
            var owmProvider = new OpenWeatherMapProvider("");
            Assert.ThrowsAsync<ArgumentException>(async () => await owmProvider.GetForecastAsync(null));
        }

        [Fact]
        public void Given_null_apiKey_throws_exception()
        {
            var owmProvider = new OpenWeatherMapProvider(null);
            Assert.ThrowsAsync<ArgumentException>(async () => await owmProvider.GetForecastAsync(null));
        }
    }
}
