﻿using Autofac;
using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using Weather.Api.Controllers;
using Weather.Services.Weather;
using Weather.Services.Weather.Model;
using Weather.Services.WeatherProviders;
using Weather.Services.WeatherProviders.CacheWeatherProvider;
using Weather.Services.WeatherProviders.OpenWeatherMap;
using Xunit;

namespace Weather.Services.Tests
{
    public class WeatherControllerTests
    {
        private readonly IContainer _services;
        private readonly IContainer _invalidApiKeyServices;

        public WeatherControllerTests()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<OpenWeatherMapProvider>().As<IWeatherProvider>().WithParameter("apiKey", "8a8b73944d61acc697760ed667fa1e56");
            builder.RegisterType<CacheWeatherProvider>().As<ICacheWeatherProvider>();
            builder.RegisterType<WeatherService>().As<IWeatherService>();
            builder.RegisterType<WeatherController>();
            _services = builder.Build();

            builder = new ContainerBuilder();
            builder.RegisterType<OpenWeatherMapProvider>().As<IWeatherProvider>().WithParameter("apiKey", Guid.NewGuid().ToString());
            builder.RegisterType<CacheWeatherProvider>().As<ICacheWeatherProvider>();
            builder.RegisterType<WeatherService>().As<IWeatherService>();
            builder.RegisterType<WeatherController>();
            _invalidApiKeyServices = builder.Build();
        }

        [Theory]
        [InlineData("Poland", "Warsaw")]
        [InlineData("Polska", "Warszawa")]
        [InlineData("Germany", "Berlin")]
        [InlineData("Deutschland", "Berlin")]
        [InlineData("Russia", "Moscow")]
        [InlineData("Россия", "Москва")]
        public async void Given_valid_location_results_200_and_forecast(string country, string city)
        {
            var controller = GetControllerWithEmptyRequest();

            var result = await controller.GetForecast(country, city);
            var response = await result.ExecuteAsync(new CancellationToken());

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var json = await response.Content.ReadAsStringAsync();
            json.Should().NotBeEmpty();

            var forecast = JsonConvert.DeserializeObject<Forecast>(json);
            forecast.Should().NotBeNull();

            forecast.Location.Should().NotBeNull();
            forecast.Location.City.Should().Be(city);
            forecast.Location.CountryCode.Should().Be(new Location { Country = country }.CountryCode);

            forecast.Temperature.Should().NotBeNull();
        }

        [Theory]
        [InlineData("Poland")]
        [InlineData("Polska")]
        [InlineData("Germany")]
        [InlineData("Deutschland")]
        [InlineData("Russia")]
        [InlineData("Россия")]
        public async void Given_invalid_location_results_404(string country)
        {
            var controller = GetControllerWithEmptyRequest();

            var result = await controller.GetForecast(country, Guid.NewGuid().ToString());
            var response = await result.ExecuteAsync(new CancellationToken());

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Given_invalid_apiKey_results_500()
        {
            var location = new Location
            {
                Country = "Poland",
                City = Guid.NewGuid().ToString()
            };

            var controller = GetControllerWithEmptyRequest(true);

            var result = await controller.GetForecast(location.Country, location.City);
            var response = await result.ExecuteAsync(new CancellationToken());

            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }

        [Fact]
        public async void Given_empty_country_results_404()
        {
            var controller = GetControllerWithEmptyRequest();

            var result = await controller.GetForecast("", null);
            var response = await result.ExecuteAsync(new CancellationToken());

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void Given_empty_city_results_404()
        {
            var location = new Location
            {
                Country = "Poland",
                City = ""
            };

            var controller = GetControllerWithEmptyRequest();

            var result = await controller.GetForecast(location.Country, location.City);
            var response = await result.ExecuteAsync(new CancellationToken());

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        private WeatherController GetControllerWithEmptyRequest(bool invalidApiKey = false)
        {
            var controller = invalidApiKey ? _invalidApiKeyServices.Resolve<WeatherController>() : _services.Resolve<WeatherController>();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            return controller;
        }
    }
}
