﻿using FluentAssertions;
using System;
using System.Security.Authentication;
using Weather.Services.Exceptions;
using Weather.Services.Weather;
using Weather.Services.Weather.Model;
using Weather.Services.WeatherProviders.OpenWeatherMap;
using Xunit;

namespace Weather.Services.Tests
{
    public class OpenWeatherMapIntegrationTests
    {
        [Theory]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Poland", "Warsaw")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Polska", "Warszawa")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Germany", "Berlin")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Deutschland", "Berlin")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Russia", "Moscow")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Россия", "Москва")]
        public async void Given_valid_apiKey_and_location_results_forecast(string apiKey, string country, string city)
        {
            var location = new Location
            {
                City = city,
                Country = country
            };

            var owmProvider = new OpenWeatherMapProvider(apiKey);
            var weatherService = new WeatherService(owmProvider, Stubs.CacheWeatherProvider);

            var result = await weatherService.GetForecastAsync(location);

            result.Should().NotBeNull();
            result.Location.Should().BeEquivalentTo(location);
            result.Temperature.Should().NotBeNull();
        }

        [Theory]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Poland")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Polska")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Germany")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Deutschland")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Russia")]
        [InlineData("8a8b73944d61acc697760ed667fa1e56", "Россия")]
        public async void Given_valid_apiKey_and_invalid_location_throws_locationNotFound(string apiKey, string country)
        {
            var location = new Location
            {
                City = Guid.NewGuid().ToString(),
                Country = country
            };

            var owmProvider = new OpenWeatherMapProvider(apiKey);
            var weatherService = new WeatherService(owmProvider, Stubs.CacheWeatherProvider);

            await Assert.ThrowsAsync<LocationNotFoundException>(async () => await weatherService.GetForecastAsync(location));
        }

        [Theory]
        [InlineData("key", "Poland", "Warsaw")]
        [InlineData("key", "Polska", "Warszawa")]
        [InlineData("key", "Germany", "Berlin")]
        [InlineData("key", "Deutschland", "Berlin")]
        [InlineData("key", "Russia", "Moscow")]
        [InlineData("key", "Россия", "Москва")]
        public async void Given_invalid_apiKey_and_valid_location_throws_exception(string apiKey, string country, string city)
        {
            var location = new Location
            {
                City = city,
                Country = country
            };

            var owmProvider = new OpenWeatherMapProvider(apiKey);
            var weatherService = new WeatherService(owmProvider, Stubs.CacheWeatherProvider);

            await Assert.ThrowsAsync<InvalidCredentialException>(async () => await weatherService.GetForecastAsync(location));
        }
    }
}
