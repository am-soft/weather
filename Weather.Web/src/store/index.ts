import { combineReducers, createStore } from 'redux';
import { WeatherReducer, WeatherState } from '../services/weather';
import { LoaderState, LoaderReducer } from '../services/loader';

export class RootState {
  public loader: LoaderState;
  public weather: WeatherState;
}

export const rootReducer = combineReducers({
  loader: LoaderReducer.build(),
  weather: WeatherReducer.build()
});

const store = createStore(rootReducer);

export default store;
