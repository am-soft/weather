import { Dispatch, AnyAction } from 'redux';

export class DispatchActionMeta {
  public dispatch: Dispatch<AnyAction>;
}
