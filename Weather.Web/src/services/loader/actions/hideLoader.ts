import { actionCreatorFactory } from 'typescript-fsa';
import { ReducerBuilder } from 'typescript-fsa-reducers';
import { LoaderState } from '..';

const actionCreator = actionCreatorFactory();

export const hideLoader = actionCreator('LOADER_HIDE');

export function buildHideLoaderReducers(reducer: ReducerBuilder<LoaderState, LoaderState>) {
  reducer.case(hideLoader, state => ({ ...state, show: false }));
}
