import { actionCreatorFactory } from 'typescript-fsa';
import { ReducerBuilder } from 'typescript-fsa-reducers';
import { LoaderState } from '..';

const actionCreator = actionCreatorFactory();

export const showLoader = actionCreator('LOADER_SHOW');

export function buildShowLoaderReducers(reducer: ReducerBuilder<LoaderState, LoaderState>) {
  reducer.case(showLoader, state => ({ ...state, show: true }));
}
