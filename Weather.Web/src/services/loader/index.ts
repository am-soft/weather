import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { showLoader, buildShowLoaderReducers } from './actions/showLoader';
import { hideLoader, buildHideLoaderReducers } from './actions/hideLoader';

export class LoaderState {
  public show: boolean;
}

const initialState: LoaderState = {
  show: false
};

export const LoaderReducer = reducerWithInitialState(initialState);

buildShowLoaderReducers(LoaderReducer);
buildHideLoaderReducers(LoaderReducer);

export const LoaderService = {
  hideLoader,
  showLoader
};
