export class ForecastFetchExcetion {
  private static getErrorMessage(statusCode?: number) {
    switch (statusCode) {
      case 404:
        return 'No forecast found for desired location';

      case 500:
        return 'There was an error during processing your request. Try again later';
    }

    return 'Unknown error';
  }

  public message: string;

  constructor(statusCode: number) {
    this.message = ForecastFetchExcetion.getErrorMessage(statusCode);
  }
}
