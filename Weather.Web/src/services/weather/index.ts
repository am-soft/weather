import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Forecast } from './dtos';
import { getForecast, buildGetForecastReducers } from './actions/getForecast';
import { clearForecast, buildClearForecastReducers } from './actions/clearForecast';

export class WeatherState {
  public pendingRequest?: boolean;
  public forecast?: Forecast;
  public fetchForecastError?: string;
}
const initialState: WeatherState = {};

export const WeatherReducer = reducerWithInitialState(initialState);

buildGetForecastReducers(WeatherReducer);
buildClearForecastReducers(WeatherReducer);

export const WeatherService = {
  clearForecast,
  getForecast
};
