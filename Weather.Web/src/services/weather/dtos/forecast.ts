import  { Location } from '.';

export class Forecast {
    public location: Location;

    public temperature: {
        format: string;
        value: number
    }

    public humidity: number;
}
