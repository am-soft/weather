import { Location } from './location';
import { Forecast } from './forecast';

export { Location, Forecast };
