import actionCreatorFactory from 'typescript-fsa';
import { WeatherState } from '..';
import { ReducerBuilder } from '../../../../node_modules/typescript-fsa-reducers/dist';

const actionCreator = actionCreatorFactory();

export const clearForecast = actionCreator('CLEAR_FORECAST');

export function buildClearForecastReducers(reducer: ReducerBuilder<WeatherState, WeatherState>) {
  reducer.case(clearForecast, state => ({ ...state, forecast: undefined }));
}
