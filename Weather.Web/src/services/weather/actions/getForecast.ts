import actionCreatorFactory from 'typescript-fsa';
import { ReducerBuilder } from 'typescript-fsa-reducers';
import { WeatherState } from '..';
import { Forecast, Location } from '../dtos';
import { DispatchActionMeta } from '../../../store/dispatchActionMeta';
import ForecastFetchExcetion from '../exceptions';
import { showLoader } from '../../loader/actions/showLoader';
import { hideLoader } from '../../loader/actions/hideLoader';

const actionCreator = actionCreatorFactory();

export const getForecast = actionCreator.async<Location, Forecast>('GET_FORECAST', DispatchActionMeta);

export function buildGetForecastReducers(reducer: ReducerBuilder<WeatherState, WeatherState>) {
  reducer.caseWithAction(getForecast.started, (state, action) => {
    const dispatch = (action.meta as DispatchActionMeta).dispatch;

    const apiUrl = 'http://localhost:51000';

    let requestFinished = false;
    let showedLoader = false;

    fetch(`${apiUrl}/api/weather/${action.payload.country}/${action.payload.city}`)
      .then(response => {
        requestFinished = true;

        if (showedLoader) {
          dispatch(hideLoader());
        }

        if (response.ok) {
          response.json().then((forecast: Forecast) =>
            dispatch(
              getForecast.done({
                params: action.payload,
                result: forecast
              })
            )
          );
        } else {
          throw new ForecastFetchExcetion(response.status);
        }
      })
      .catch(error => {
        if (showedLoader) {
          dispatch(hideLoader());
        }

        const errorMessage = error instanceof ForecastFetchExcetion ? error.message : `Could not get weather forecast: ${error.message}`;

        dispatch(
          getForecast.failed({
            error: errorMessage,
            params: action.payload
          })
        );
      });

    // delay showing loader to avoid flashing
    setTimeout(() => {
      if (!requestFinished) {
        showedLoader = true;
        dispatch(showLoader());
      }
    }, 300);

    return { ...state, fetchForecastError: undefined, pendingRequest: true };
  });

  reducer.caseWithAction(getForecast.done, (state, action) => {
    return { ...state, forecast: action.payload.result, pendingRequest: false };
  });

  reducer.caseWithAction(getForecast.failed, (state, action) => {
    return { ...state, fetchForecastError: action.payload.error as string, pendingRequest: false };
  });
}
