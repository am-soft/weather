import './App.css';
import AppIcon from './assets/app-icon.png';

import * as React from 'react';
import { MapStateToProps, connect } from 'react-redux';
import { Forecast } from './services/weather/dtos';
import { RootState } from './store';

import Form from './components/Form';
import WeatherPresenter from './components/WeatherPresenter';
import Loader from './components/Loader';

class StateProps {
  public forecast?: Forecast;
}

type Props = StateProps;

class App extends React.Component<Props> {
  public static mapStateProps: MapStateToProps<StateProps, {}, RootState> = state => ({
    forecast: state.weather.forecast
  });

  public render() {
    return (
      <div className="App">
        <Loader />

        <header className="App-header">
          <img src={AppIcon} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Weather App</h1>
        </header>

        {this.props.forecast ? <WeatherPresenter forecast={this.props.forecast} /> : <Form />}
      </div>
    );
  }
}

export default connect<StateProps>(App.mapStateProps)(App);
