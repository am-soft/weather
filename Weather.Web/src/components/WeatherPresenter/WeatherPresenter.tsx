import './WeatherPresenter.css';

import TemperatureHigh from '../../assets/temperature-high.png';
import TemperatureLow from '../../assets/temperature-low.png';

import * as React from 'react';
import { MapDispatchToProps, connect } from 'react-redux';
import { Forecast } from '../../services/weather/dtos';
import { WeatherService } from '../../services/weather';

class OwnProps {
  public forecast: Forecast;
}

class DispatchProps {
  public getNewForecast: () => void;
}

type Props = OwnProps & DispatchProps;

class WeatherPresenter extends React.Component<Props> {
  public static mapDispatchProps: MapDispatchToProps<DispatchProps, {}> = dispatch => ({
    getNewForecast: () => dispatch(WeatherService.clearForecast())
  });

  public render() {
    const { forecast } = this.props;

    return (
      <div className="weather-presenter container">
        <h2>Weather Forecast</h2>
        <h3>
          {forecast.location.city}, {forecast.location.country}
        </h3>
        <div className="forecast">
          <div>
            <img className="temperature-icon" src={this.getTemperatureIcon()} />
          </div>
          <div>
            <span className="descriptor">Temperature:</span>
            <span className="value">
              {forecast.temperature.value}
              {this.getTemperatureFormatSign()}
            </span>
          </div>

          <div>
            <span className="descriptor">Humidity:</span>
            <span className="value">{forecast.humidity}%</span>
          </div>

          <div>
            <button onClick={() => this.props.getNewForecast()}>Get new forecast</button>
          </div>
        </div>
      </div>
    );
  }

  private getTemperatureIcon() {
    return this.props.forecast.temperature.value >= 5 ? TemperatureHigh : TemperatureLow;
  }

  private getTemperatureFormatSign() {
    switch (this.props.forecast.temperature.format) {
      case 'Celsius':
        return '°C';

      case 'Fahrenheit':
        return '°F';
    }

    return '';
  }
}

export default connect<{}, DispatchProps>(
  null,
  WeatherPresenter.mapDispatchProps
)(WeatherPresenter);
