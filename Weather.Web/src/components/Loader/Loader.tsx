import './Loader.css';

import * as React from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { LoaderState } from '../../services/loader';
import { RootState } from '../../store';

type Props = LoaderState;

class Loader extends React.Component<Props> {
  public static mapStateProps: MapStateToProps<LoaderState, {}, RootState> = state => state.loader;

  public componentWillReceiveProps(props: Props) {
    console.log('mount', props);
  }

  public render() {
    return this.props.show ? this.loader() : null;
  }

  private loader() {
    return (
      <div className="loader-wrapper">
        <div className="loader" />
      </div>
    );
  }
}

export default connect<LoaderState>(Loader.mapStateProps)(Loader);
