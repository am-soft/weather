import './Form.css';

import * as React from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { RootState } from '../../store';
import { WeatherService } from '../../services/weather';
import { Location } from '../../services/weather/dtos';

class FormField {
  public value?: string;
  public invalid?: boolean;
}

class State {
  public city = new FormField();
  public country = new FormField();
}

class StateProps {
  public fetchForecastError?: string;
  public pendingRequest?: boolean;
}

class DispatchProps {
  public getForecast: (location: Location) => void;
}

type Props = StateProps & DispatchProps;

export class Form extends React.Component<Props, State> {
  public static mapStateProps: MapStateToProps<StateProps, {}, RootState> = state => ({
    fetchForecastError: state.weather.fetchForecastError,
    pendingRequest: state.weather.pendingRequest
  });

  public static mapDispatchProps: MapDispatchToProps<DispatchProps, RootState> = dispatch => ({
    getForecast: location => dispatch(WeatherService.getForecast.started(location, { dispatch }))
  });

  constructor(props: Props) {
    super(props);
    this.state = new State();
  }

  public render() {
    return (
      <div className="form container">
        <p className="App-intro">Fill in the form below to get the current weather forecast for desired location</p>

        <div className="result">
          {this.props.fetchForecastError == null ? null : <div className="color-error">{this.props.fetchForecastError}</div>}
        </div>

        <div>
          <input
            type="text"
            placeholder="Country"
            className={`${this.state.country.invalid ? 'invalid' : ''}`}
            onChange={e => this.setState({ country: { value: e.target.value, invalid: false } })}
            onKeyDown={e => this.onKeyDown(e)}
          />
          <input
            type="text"
            placeholder="City"
            className={`${this.state.city.invalid ? 'invalid' : ''}`}
            onChange={e => this.setState({ city: { value: e.target.value, invalid: false } })}
            onKeyDown={e => this.onKeyDown(e)}
          />
          <button disabled={this.props.pendingRequest} onClick={() => this.getForecast()}>
            Get forecast
          </button>
        </div>
      </div>
    );
  }

  private onKeyDown(event: React.KeyboardEvent) {
    if (event.key === 'Enter') {
      this.getForecast();
    }
  }

  private validateFormField(field: FormField): boolean {
    const valid = field.value != null && field.value.length > 0;
    field.invalid = !valid;
    return valid;
  }

  private getForecast() {
    if (!this.validateFormField(this.state.country) || !this.validateFormField(this.state.city)) {
      this.setState({ country: this.state.country, city: this.state.city });
      return;
    }

    this.props.getForecast(new Location(this.state.country.value as string, this.state.city.value as string));
  }
}

export default connect<StateProps, DispatchProps>(
  Form.mapStateProps,
  Form.mapDispatchProps
)(Form);
