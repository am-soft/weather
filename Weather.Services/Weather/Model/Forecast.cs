﻿using System;

namespace Weather.Services.Weather.Model
{
    public class Forecast
    {
        public Location Location { get; set; }

        public Temperature Temperature { get; set; }

        public decimal Humidity { get; set; }

        public DateTime Date { get; set; }

        public void EnsureIsValid()
        {
            if (Location == null)
                throw new ArgumentException("Location is null");
            Location.EnsureIsValid();

            if (Temperature == null)
                throw new ArgumentException("Location is null");
            Temperature.EnsureIsValid();

            if (Date <= DateTime.MinValue)
                throw new ArgumentException("Date is invalid");
        }
    }
}
