﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Weather.Services.Weather.Model
{
    public class Location
    {
        private static Dictionary<string, string> CountryCodes = BuildContryCodesDictionary();

        private string _country;

        public string Country
        {
            get
            {
                return _country;
            }

            set
            {
                _country = value;
                CountryCode = CountryCodes.ContainsKey(_country.ToLower()) ? CountryCodes[_country.ToLower()] : "";
            }
        }

        public string CountryCode { get; private set; }

        public string City { get; set; }

        private static Dictionary<string, string> BuildContryCodesDictionary()
        {
            var dictionary = new Dictionary<string, string>();
            var cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            foreach(var culture in cultures)
            {
                var regionInfo = new RegionInfo(culture.LCID);

                if (!dictionary.ContainsKey(regionInfo.EnglishName.ToLower()))
                    dictionary[regionInfo.EnglishName.ToLower()] = regionInfo.TwoLetterISORegionName;

                if (!dictionary.ContainsKey(regionInfo.DisplayName.ToLower()))
                    dictionary[regionInfo.DisplayName.ToLower()] = regionInfo.TwoLetterISORegionName;

                if (!dictionary.ContainsKey(regionInfo.NativeName.ToLower()))
                    dictionary[regionInfo.NativeName.ToLower()] = regionInfo.TwoLetterISORegionName;
            }

            return dictionary;
        }

        public void EnsureIsValid()
        {
            if (string.IsNullOrEmpty(Country))
                throw new ArgumentException("Country is null or empty");

            if (string.IsNullOrEmpty(CountryCode))
                throw new ArgumentException("Country Code is null or empty");

            if (string.IsNullOrEmpty(City))
                throw new ArgumentException("City is null or empty");
        }
     }
}
