﻿using System;

namespace Weather.Services.Weather.Model
{
    public class Temperature
    {
        public string Format { get; set; }

        public decimal Value { get; set; }

        public void EnsureIsValid()
        {
            if (string.IsNullOrEmpty(Format))
                throw new ArgumentException("Format is null or empty");
        }
    }
}
