﻿using System;
using System.Threading.Tasks;
using Weather.Services.Weather.Model;
using Weather.Services.WeatherProviders;
using Weather.Services.WeatherProviders.CacheWeatherProvider;

namespace Weather.Services.Weather
{
    public class WeatherService : IWeatherService
    {
        private readonly IWeatherProvider _weatherProvider;
        private readonly ICacheWeatherProvider _cacheWeatherProvider;

        public WeatherService(IWeatherProvider weatherProvider, ICacheWeatherProvider cacheWeatherProvider)
        {
            _weatherProvider = weatherProvider;
            _cacheWeatherProvider = cacheWeatherProvider;
        }

        public async Task<Forecast> GetForecastAsync(Location location)
        {
            if (location == null)
                throw new ArgumentException("Location is null");

            location.EnsureIsValid();

            var result = await _cacheWeatherProvider.GetForecastAsync(location);

            if(result == null)
            {
                result = await _weatherProvider.GetForecastAsync(location);
                _cacheWeatherProvider.CacheForecast(result);
            }

            return result;
        }
    }
}
