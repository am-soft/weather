﻿using System.Threading.Tasks;
using Weather.Services.Weather.Model;

namespace Weather.Services.Weather
{
    public interface IWeatherService
    {
        Task<Forecast> GetForecastAsync(Location location);
    }
}
