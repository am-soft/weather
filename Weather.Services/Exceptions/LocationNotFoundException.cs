﻿namespace Weather.Services.Exceptions
{
    public class LocationNotFoundException : WeatherServiceException
    {
        public LocationNotFoundException() : base("Weather forecast not found for desired location")
        {
        }
    }
}
