﻿namespace Weather.Services.Exceptions
{
    public class WeatherProviderException : WeatherServiceException
    {
        public WeatherProviderException(string message) : base($"Weather provider error: {message}")
        {
        }
    }
}
