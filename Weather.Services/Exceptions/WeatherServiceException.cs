﻿using System;

namespace Weather.Services.Exceptions
{
    public class WeatherServiceException : Exception
    {
        public WeatherServiceException(string message) : base(message)
        {
        }
    }
}
