﻿using System.Threading.Tasks;
using Weather.Services.Weather.Model;

namespace Weather.Services.WeatherProviders
{
    public interface IWeatherProvider
    {
        Task<Forecast> GetForecastAsync(Location location);
    }
}
