﻿using System;
using Weather.Services.Weather.Model;

namespace Weather.Services.WeatherProviders.CacheWeatherProvider
{
    public interface ICacheWeatherProvider : IWeatherProvider, IDisposable
    {
        void CacheForecast(Forecast forecast);
    }
}
