﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Weather.Services.Weather.Model;

namespace Weather.Services.WeatherProviders.CacheWeatherProvider
{
    public class CacheWeatherProvider : ICacheWeatherProvider
    {
        private static List<Forecast> CachedForecasts = new List<Forecast>();

        public static TimeSpan MaxAcceptedForecastDateDifference = new TimeSpan(3, 0, 0);

        public void CacheForecast(Forecast forecast)
        {
            if (forecast == null)
                throw new ArgumentException("Forecast is null");

            forecast.EnsureIsValid();

            CachedForecasts.Add(forecast);
        }

        public Task<Forecast> GetForecastAsync(Location location)
        {
            var maxAcceptedDate = DateTime.UtcNow.Add(MaxAcceptedForecastDateDifference);

            var cachedForecast = CachedForecasts
                .Where(forecast => forecast.Location.CountryCode.ToLower() == location.CountryCode.ToLower() && forecast.Location.City.ToLower() == location.City.ToLower())
                .Where(forecast => forecast.Date <= maxAcceptedDate)
                .OrderBy(forecast => forecast.Date)
                .FirstOrDefault();

            return Task.FromResult(cachedForecast);
        }

        public void Dispose()
        {
            CachedForecasts.Clear();   
        }
    }
}
