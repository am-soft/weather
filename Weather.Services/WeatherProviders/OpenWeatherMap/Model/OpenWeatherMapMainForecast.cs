﻿using Newtonsoft.Json;

namespace Weather.Services.WeatherProviders.OpenWeatherMap.Model
{
    public partial class OpenWeatherMapMainForecast
    {
        [JsonProperty("temp")]
        public decimal Temp { get; set; }

        [JsonProperty("humidity")]
        public decimal Humidity { get; set; }
    }
}
