﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Weather.Services.WeatherProviders.OpenWeatherMap.Model
{
    public class OpenWeatherMapResponse
    {
        [JsonProperty("cod")]
        public int ResponseCode { get; set; }

        [JsonProperty("list")]
        public IEnumerable<OpenWetherMapForecast> List { get; set; }
    }
}
