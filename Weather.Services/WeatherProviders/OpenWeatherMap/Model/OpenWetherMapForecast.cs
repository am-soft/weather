﻿using Newtonsoft.Json;
using System;

namespace Weather.Services.WeatherProviders.OpenWeatherMap.Model
{
    public partial class OpenWetherMapForecast
    {
        [JsonProperty("dt")]
        public long UnixDate { get; set; }

        [JsonProperty("dt_txt")]
        public string DateString { get; set; }

        [JsonProperty("main")]
        public OpenWeatherMapMainForecast Weather { get; set; }

        public DateTime? Date { get; set; }
    }
}