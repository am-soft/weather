﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading.Tasks;
using Weather.Services.Exceptions;
using Weather.Services.Weather.Model;
using Weather.Services.WeatherProviders.OpenWeatherMap.Model;

namespace Weather.Services.WeatherProviders.OpenWeatherMap
{
    public class OpenWeatherMapProvider : IWeatherProvider
    {
        private static string ApiUrl = "http://api.openweathermap.org/data/2.5/forecast";

        private readonly string _apiKey;

        public OpenWeatherMapProvider(string apiKey)
        {
            _apiKey = apiKey;
        }

        public async Task<Forecast> GetForecastAsync(Location location)
        {
            if (string.IsNullOrEmpty(_apiKey))
                throw new InvalidCredentialException("API key is null or empty");

            HttpResponseMessage response;

            using (var httpClient = new HttpClient())
            {
                response = await httpClient.GetAsync(
                    $"{ApiUrl}?q={UrlEncode(location.City)},{UrlEncode(location.CountryCode)}&units=metric&APPID={_apiKey}"
                    );
            }

            var json = await response.Content.ReadAsStringAsync();

            var owmResponse = JsonConvert.DeserializeObject<OpenWeatherMapResponse>(json);

            if (owmResponse == null)
                throw new WeatherProviderException("Unknown response from OpenWeatherMap server");

            if (owmResponse.ResponseCode == (int)HttpStatusCode.Unauthorized)
                throw new InvalidCredentialException("API key is invalid");

            if (owmResponse.ResponseCode == (int)HttpStatusCode.NotFound)
                throw new LocationNotFoundException();

            if (owmResponse.ResponseCode != (int)HttpStatusCode.OK)
                throw new WeatherProviderException("OpenWeatherMap server result is not success");

            foreach (var forecast in owmResponse.List)
            {
                forecast.Date = DateTime.Parse(forecast.DateString);
            }

            var nearestForecast = owmResponse
                .List
                .Where(x => x.Date != null)
                .OrderBy(x => x.Date)
                .FirstOrDefault(forecast => forecast.Date >= DateTime.UtcNow);

            if (nearestForecast == null)
                throw new WeatherProviderException("OpenWeatherMap did not return valid forecast");

            return new Forecast
            {
                Location = location,
                Date = nearestForecast.Date.Value,
                Temperature = new Temperature
                {
                    Format = "Celsius",
                    Value = nearestForecast.Weather.Temp
                },
                Humidity = nearestForecast.Weather.Humidity
            };
        }

        private static string UrlEncode(string value)
        {
            return value.Replace(" ", "+");
        }
    }
}
